
from minio.error import S3Error
class BucketService:
    def __init__(self, minio_client):
      self.client = minio_client

    def checkIfBucketExist(self,bucket_name):
        return self.client.bucket_exists(bucket_name)

    def createBucket(self,bucket_name):
        try:
            self.client.make_bucket(bucket_name)
        except S3Error as exc:
            print("error occurred.", exc)
        

    def getBuckets(self):
        pass

    def deleteBucket(self, bucket_name):
        pass