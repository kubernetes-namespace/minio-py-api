from minio import Minio
import os
from dotenv import load_dotenv
load_dotenv()

class InitService:
    def __init__(self,
                 url=os.environ.get("MINIO_ENPOINT"),
                 access_key=os.environ.get("MINIO_ACCESS_KEY"),
                 sercret_key=os.environ.get("MINIO_SECRET_KEY"),
                 session_token=os.environ.get("MINIO_SESSION_TOKEN"),
                 secure=os.environ.get("MINIO_SECURE"),
                 region=os.environ.get("MINIO_REGION"),
                 http_client=os.environ.get("MINIO_HTTP_CLIENT"),
                 credentials=os.environ.get("MINIO_CREDENTIALS")
                 ):
        self.url = url
        self.access_key = access_key
        self.secret_key = sercret_key
        self.sessio_token = session_token
        self.secure =secure
        self.region = region
        self.http_client = http_client
        self.credentials = credentials

    def get_base_connexion(self):
        return  Minio(self.url,self.access_key,self.secret_key)