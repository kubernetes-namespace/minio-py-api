from minio.error import S3Error

class ObjectService:
    def __init__(self, minio_client):
        self.client = minio_client
    
    def fget_object(self,bucket_name,object_name,file_name):
        pass

    def copy_object(self):
        pass

    def put_object(self):
        pass

    def fput_object(self,bucket_name,object_name,file_path,content_type="application/octet-stream", metadata=None, sse=None, progress=None, part_size=0, num_parallel_uploads=3, tags=None, retention=None, legal_hold=False):
        self.client.fput_object(
            bucket_name,
            object_name, 
            file_path)

    def remove_object(self, bucket_name,object_name,version=None):
        pass
