from ast import arg
from http import client
from flask import request
from MinioRestFullApi.api.api_def import api
from flask_restplus import Resource
from MinioRestFullApi.api.endpoints.endpoints_def import page_with_client, client
from MinioRestFullApi.api.parsers import pagination_parser as pagination

namespace = api.namespace('Clients', descriprion='a list of operation for Client')

@namespace.route('/')
class Client(Resource):
    @api.expect(pagination)
    @api.marshal_with(page_with_client)
    def get(self):
        args = pagination.parse_args(request)
        page = args.get('page', 1)
        items_per_page = args.get('items_per_page',5)
        return clients

    @api.expect(client)
    def post(self):
        return None , 200
