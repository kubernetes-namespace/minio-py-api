
from flask_restplus import fields
from MinioRestFullApi.api.api_def import api

client = api.model('Client', {
    'client_id': fields.String(readOnly=True, description='Id of client'),
    'client_secret': fields.String(required=True, unique=True, description='api client Secret')
})

bucket = api.model('Bucket', {
    'id': fields.Integer(readOnly=True, description='Id of bucket'),
    'name': fields.String(required=True, unique=True, description='bucket name'),
    'client_api': fields.String(attribute='clients.client_id'),
})

pagination = api.model('One page buckets', {
    'page': fields.Integer(description='Current page'),
    'pages': fields.Integer(description='Total Pages'),
    'item_per_page': fields.Integer(description='Item per Page'),
    'total_items': fields.Integer(description='Total amount of items')
})

page_with_buckets = api.inherit('Page with Buckets', pagination, {
    'items': fields.List(fields.Nested(bucket))
})

page_with_client = api.inherit('Page with Clients', pagination, {
    'items': fields.List(fields.Nested(client))
})