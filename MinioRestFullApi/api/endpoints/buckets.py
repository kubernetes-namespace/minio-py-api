from ast import arg
from flask import request
from MinioRestFullApi.api.api_def import api
from flask_restplus import Resource
from MinioRestFullApi.api.endpoints.endpoints_def import page_with_buckets, bucket
from MinioRestFullApi.api.parsers import pagination_parser as pagination

from MinioRestFullApi.database.dtos import Bucket
from MinioRestFullApi.api.logic.bucket import create_bucket

namespace = api.namespace('buckects', descriprion='a list of operation for bucket')

@namespace.route('/')
class Buckets(Resource):
    @api.expect(pagination)
    @api.marshal_with(page_with_buckets)
    def get(self):
        args = pagination.parse_args(request)
        page = args.get('page', 1)
        items_per_page = args.get('items_per_page',5)
        buckets = Bucket.query.paginate(page, items_per_page,error_out=False)
        return buckets

    @api.expect(bucket)
    def post(self):
        create_bucket(request.json)
        return None , 200
