FLASK_SERVER_NAME =' localhost:1337'
FLASK_THREADED = True
FLASK_DEBUG = True  # 'change before deployment'


RESTPLUS_MASK_SWAGGER = 'list'
RESTPLUS_VALIDATE = True
SWAGGER_UI_DOC_EXPANSION = False

SQLALCHEMY_DATABASE_URI = 'sqlite:///minio_api.db'
SQLALCHEMY_TRACK_MODIFICATIONS= False

