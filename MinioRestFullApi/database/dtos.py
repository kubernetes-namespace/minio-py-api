from MinioRestFullApi.database.db import db


class Client(db.Model):
    client_id = db.Column(db.String(255), primary_key=True)
    client_secret = db.Column(db.String(255))
    buckets = db.relationship('Bucket', backref='client', lazy=True)
    
    def __init__(self, client_id, client_secret=None ):
        self.client_id = client_id
                        
        if client_secret is not None :
            self.client_secret = client_secret

class Bucket(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    client_api = db.Column(db.String, db.ForeignKey('client.id'), nullable=False) 
    
    def __init__(self, name, client_api):
        self.name = name
        if isinstance(client_api, str):
            self.client_api = client_api
        else:
            raise ValueError(f"unsupported str format: {client_api}")
